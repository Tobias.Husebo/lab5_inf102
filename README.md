# Lab 5 - Undirected Graphs
In this task you will be coding a graph datastructure implementation [Adjacency Set](https://static.javatpoint.com/ds/images/graph-representation4.png) which is an AdjacencyList that uses sets instead of lists.

The graph in this assignment are **undirected** and **unweighted**:

`Main.java` contains some code to do simple checks for the graph implementation.

### Task
Implement the interface `IGraph` in `AdjacencySet.java`. Methods:
 * `size()`
 * `addNode(V node)`
 * `removeNode(V node)`
 * `addEdge(V u, v v)`
 * `removeEdge(V u, V v)`
 * `hasNode(V node)`
 * `hasEdge(V u, V v)`
 * `getNeighbourhood(V node)`
 * `connected(V u, V v)`

✅ Run `AdjacencySetTest` to check your solution. The task is passed if all tests pass.


