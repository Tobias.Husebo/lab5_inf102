package INF102.lab5.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class AdjacencySet<V> implements IGraph<V> {
    private HashMap<V, Set<V>> Adj_set;

    public AdjacencySet(){
        this.Adj_set = new HashMap<V, Set<V>>(); 
    }

    @Override
    public int size() {
        return Adj_set.size();
    }

    @Override
    public void addNode(V node) {
        HashSet<V> SetOfNodes = new HashSet<V>();
        Adj_set.put(node, SetOfNodes);
    }

    @Override
    public void removeNode(V node) {
        Adj_set.remove(node);
    }

    @Override
    public void addEdge(V u, V v) {
        Adj_set.get(u).add(v);
        Adj_set.get(v).add(u);
    }

    @Override
    public void removeEdge(V u, V v) {
        Adj_set.get(u).remove(v);
        Adj_set.get(v).remove(u);
    }

    @Override
    public boolean hasNode(V node) {
        return Adj_set.containsKey(node);
    }

    @Override
    public boolean hasEdge(V u, V v) {
        return Adj_set.get(u).contains(v);
    }

    @Override
    public Set<V> getNeighbourhood(V node) {
        return Adj_set.get(node);
    }

    @Override
    public boolean connected(V u, V v) {
        if (hasEdge(u, v)){
            return true;
        }
        HashMap<V, Boolean> map = new HashMap<>();
        for (V node : Adj_set.keySet()) {
            map.put(node, false);
        }
        return searchForConnectivity(u, v, map);
    }

    private boolean searchForConnectivity(V u, V v, HashMap<V, Boolean> map) {
        for (V node1 : getNeighbourhood(u)){
            deepSearch(node1, map);
        }
        return map.get(v);
    }

    private void deepSearch(V u, HashMap<V, Boolean> map) {
        for (V node : getNeighbourhood(u)){
            if (!(map.get(node))){
                map.replace(node, false, true);
                deepSearch(node, map);
            }
        }
    }
}